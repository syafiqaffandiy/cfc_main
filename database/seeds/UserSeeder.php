<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new App\User();
        $user->password = bcrypt('1234');
        $user->email = 'admin@test.com';
        $user->name = 'Test Admin';
        $user->role = 'admin';
        $user->save();

        \App\UserRole::create([
            'user_id' => $user->id,
            'role_id' => 1
        ]);


        $user = new App\User();
        $user->password = bcrypt('1234');
        $user->email = 'auditor@test.com';
        $user->name = 'Test Auditor';
        $user->role = 'auditor';
        $user->save();

        \App\UserRole::create([
            'user_id' => $user->id,
            'role_id' => 2
        ]);

        $user = new App\User();
        $user->password = bcrypt('1234');
        $user->email = 'concessionaire@test.com';
        $user->name = 'Test Concessionaire';
        $user->role = 'concessionaire';
        $user->save();

        \App\UserRole::create([
            'user_id' => $user->id,
            'role_id' => 3
        ]);
    }
}
