@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/pages/contact.js"></script>
@stop
@section('styles')
    <style>

    </style>
@stop

@section('content')
    <div class="main-content contact-page">
        <div class="row"><div class="col-md-12 col-lg-6 col-xl-3"><a href="#" class="dashbox"><i class="icon-fa icon-fa-list-ul text-primary"></i> <span class="title">
                      35
                    </span> <span class="desc">
                      Total Assessment
                    </span></a></div> <div class="col-md-12 col-lg-6 col-xl-3"><a href="#" class="dashbox"><i class="icon-fa icon-fa-ellipsis-h text-success"></i> <span class="title">
                      200
                    </span> <span class="desc">
                      Pending Approval
                    </span></a></div> <div class="col-md-12 col-lg-6 col-xl-3"><a href="#" class="dashbox"><i class="icon-fa icon-fa-send text-danger"></i> <span class="title">
                      100
                    </span> <span class="desc">
                      New Assessment
                    </span></a></div> <div class="col-md-12 col-lg-6 col-xl-3"><a href="#" class="dashbox"><i class="icon-fa  text-info icon-fa-star-half-empty"></i> <span class="title">
                      59
                    </span> <span class="desc">
                      Approve/Reject
                    </span></a></div></div>

        <div class="row contact-box">
            <div class="contact-list">
                <table id="contact-datatable" class="table table-sm table-striped table-bordered" cellspacing="0" width="100%">
                    <tfoot>
                    <tr>
                        <th></th>

                        <th></th>
                        <th>Concessionaire</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Status</th>

                    </tr>
                    </tfoot>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Assessment Title</th>
                        <th>Concessionaire</th>
                        <th>Certification/Award</th>
                        <th>Auditor</th>
                        <th>Date Report</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $status = array(
                                            "0"=>array('title'=>'Pending Score','class'=>'badge-warning'),
                                            "1"=>array('title'=>'Pending Application','class'=>'badge-warning'),
                                            "2"=>array('title'=>'Pending Result','class'=>'badge-warning'),
                                            "3"=>array('title'=>'Approve','class'=>'badge-success'),
                                            "4"=>array('title'=>'Draft','class'=>'badge-info'),
                                            "5"=>array('title'=>'Reject','class'=>'badge-danger')



                        );
                    ?>
                    @foreach($projects as $project)
                        <tr>
                            <td>{{ $project->id }}</td>
                            <td><p>Highway Name : {{ $project->name }}</p><p>Route Name : {{ $project->route }}</p><p>Chainage/Kilometer :{{ $project->kilometer_from }},{{ $project->kilometer_to }}</p></td>
                            <td>{{$project->Concessionaire}}</td>
                            <td>{{ $project->award }}</td>
                            <td>{{$project->auditor}}</td>
                            <td>{{ $project->open_to_public }}</td>
                            <td>
                                <?php
                                    if(key_exists($project->status,$status)){
                                        ?>
                                    <span class="badge <?php echo $status[$project->status]['class']; ?>"><?php echo $status[$project->status]['title']; ?></span>
                                <?php

                                    }else{
                                        ?>
                                        Not In
                                <?php
                                    }
                                ?>

                            </td>


                            <td>
                                <a href="/admin/projects/{{ $project->id }}" class="btn btn-default btn-xs">View</a>
                                <a href="/admin/projects/{{ $project->id }}" class="btn btn-default btn-xs">Edit</a>
                                <a href="/admin/projects/{{ $project->id }}" class="btn btn-default btn-xs">Delete</a>
                                <a href="/admin/projects/{{ $project->id }}" class="btn btn-default btn-xs">Download</a>



                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@stop
