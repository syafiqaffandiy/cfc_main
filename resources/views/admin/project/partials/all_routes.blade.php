<div class="row" id="all_routes_body">
    <div class="col-12">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Concessionaire</th>
                    <th>Route Name</th>
                    <th>Chainage/Kilomtre</th>
                    <th>Scope1</th>
                    <th>Scope2</th>
                    <th>Scope3</th>
                    <th>All Scope</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>#</td>
                    <td>Concessionaire</td>
                    <td>Route Name</td>
                    <td>Chainage/Kilomtre</td>
                    <td>Scope1</td>
                    <td>Scope2</td>
                    <td>Scope3</td>
                    <td>All Scope</td>
                </tr>
                <tr>
                    <td>#</td>
                    <td>Concessionaire</td>
                    <td>Route Name</td>
                    <td>Chainage/Kilomtre</td>
                    <td>Scope1</td>
                    <td>Scope2</td>
                    <td>Scope3</td>
                    <td>All Scope</td>
                </tr>
                <tr>
                    <td>#</td>
                    <td>Concessionaire</td>
                    <td>Route Name</td>
                    <td>Chainage/Kilomtre</td>
                    <td>Scope1</td>
                    <td>Scope2</td>
                    <td>Scope3</td>
                    <td>All Scope</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-12">
        <div id="allRoute_amChart" style="width:100%; height : 400px"></div>
    </div>
</div>