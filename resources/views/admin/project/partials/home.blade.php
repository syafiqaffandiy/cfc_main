<div class="row">
    <!-- separator between navigation and project head -->
    <br>
</div>
<div class="row">
    <div class="col-6">
        <img data-action="zoom" class="img-center img-fluid" src="{{ $project->image }}">
        <form action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row center">
                <div class="col-md-6">
                    <input type="file" name="image" class="form-control">
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success">Upload Image</button>
                </div>
            </div>
        </form>

        @if(!empty($project->work_program))
            <a href="{{ $project->work_program }}" target="_blank">Download work program</a>
        @else
            <br>
        @endif
        <form action="{{ route('pdf.upload_work_program.post') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row center">
                <div class="col-md-6">
                    <input type="file" name="pdf" class="form-control">
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success">Upload Work Program</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-6">
        <form action="/admin/projects" method="POST" class="form-group">
            @csrf
            <input type="hidden" name="id" value="{{ $project->id }}">
        <table class="table table-sm">
            <div style="display:none" id="register_variable">{{ json_encode($highwaylist) }}</div>
            <tr>
                <td>Highway Name</td>
                <td colspan="2">
                    <select id="highway_register"  name="name" class="form-control" value="default">
                        <option readonly value="default">Select Highway Name</option>
                        @for ($i = 0; $i < count($highwaylist); $i ++)
                            @if ($i == 0)
                                <option value="{{ $highwaylist[$i]->highwayname }}">{{ $highwaylist[$i]->highwayname }}</option>
                            @elseif ($highwaylist[$i]->highwayname != $highwaylist[$i-1]->highwayname)
                                <option value="{{ $highwaylist[$i]->highwayname }}"
                                        @if($highwaylist[$i]->highwayname == $project->name)
                                            selected
                                        @endif
                                >{{ $highwaylist[$i]->highwayname }}</option>
                            @endif
                        @endfor
                    </select>
                </td>
                <!-- <td colspan="2"><input type="text" id="name" name="name" value="{{ $project['name'] }}" class="form-control"></td> -->
            </tr>
            <tr>
                <td>Route Name</td>
                <td colspan="2">
                    <select id="route_register" name="route" class="form-control">
                        @foreach($highwaylist as $highway)
                            @if($highway->highwayname == $project->name)
                                <option value='{{ $highway->routename }}'
                                    @if($project->route == $highway->routename)
                                        selected
                                    @endif
                                >{{ $highway->routename }}</option>
                            @endif
                        @endforeach
                    </select>
                </td>
                <!-- <td colspan="2"><input type="text" id="name" name="route" value="{{ $project['route'] }}" class="form-control"></td> -->
            </tr>
            <tr>
                <td rowspan="2">Chainage/Kilometer</td>
                <td>From</td>
                <td>To</td>
            </tr>
            <tr>
                <td><input type="text" readonly id="from_register" name="kilometer_from" value="{{ $project->kilometer_from }}" class="form-control"></td>
                <td><input type="text" readonly id="to_register" name="kilometer_to" value="{{ $project->kilometer_to }}" class="form-control"></td>
            </tr>

            <tr>
                <td>Highway Length</td>
                <td colspan="2"><input type="text" id="name" name="length" value="{{ $project['length'] }}" class="form-control"></td>
            </tr>
            <tr>
                <td>Planning Highway:</td>
                <td colspan="2"><input type="text" id="name" name="planning_percentage" value="{{ $project['planning_percentage'] }}" class="form-control"></td>
            </tr>
            <tr>
                <td>Construction Highway</td>
                <td colspan="2"><input type="text" id="name" name="construction_percentage" value="{{ $project['construction_percentage'] }}" class="form-control"></td>
            </tr>
            <tr>
                <td>Existing Highway</td>
                <td colspan="2"><input type="text" id="name" name="existing_percentage" value="{{ $project['existing_percentage'] }}" class="form-control"></td>
            </tr>
            <tr>
                <td>Construction Start Date</td>
                <td colspan="2"><input type="text" id="name" name="construction_start" value="{{ $project['construction_start'] }}" class="form-control"></td>
            </tr>
            <tr>
                <td>Const. Completion Date</td>
                <td colspan="2"><input type="text" id="name" name="construction_completion" value="{{ $project['construction_completion'] }}" class="form-control"></td>
            </tr>
            <tr>
                <td>Open to Public Date</td>
                <td colspan="2"><input type="text" id="name" name="open_to_public" value="{{ $project['open_to_public'] }}" class="form-control"></td>
            </tr>
            <tr>
                <td>Report Period</td>
                <td>
                    <input type="text" id="report_period" name="report_period" value="{{ $project['report_period'] }}" class="form-control">
                </td>
                <td>year</td>
            </tr>
            <tr>
                <td>Year of assessment</td>
                <td>
                    <input type="text" id="assessment_date" name="assessment_year" value="{{ $project['assessment_year'] }}" class="form-control">
                </td>
                <td></td>
            </tr>
            @if(!empty($project->assessment_years))
                @foreach($project->assessment_years as $key => $assessment_year)
                    @if($assessment_year == $project['assessment_year'])
                        @continue
                    @endif
                    <tr>
                        <td>Year of assessment</td>
                        <td>
                            <input id="assessment_years[]" name="assessment_years[]" type="text" class="form-control" value="{{ $assessment_year }}">
                        </td>
                        <td>
                            @if(Auth::user()->isAdmin())
                                <button type="button" class="btn btn-warning" onclick="deleteAssessmentYear({{ $key }})">
                                    Delete
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endif
            <tr id="addAssessmentYearButton">
                <td colspan="3">
                    <button type="button" class="btn btn-primary" onclick="addAssessmentYear()">
                        Add New Assessment Year
                    </button>
                </td>
            </tr>
        </table>
        <evaluators
        >
        </evaluators>
        <div class="w-100">
            <div class="float-right">
                <button type="button" class="btn btn-primary" onclick="$('#guideline_navigation_tab').trigger('click')">Start Calculation</button>
            </div>
            <div class="float-right" style="margin-right: 10px">
                <input type="submit" class="btn btn-primary" value="Save">
            </div>
        </div>
        </form>
    </div>
</div>
