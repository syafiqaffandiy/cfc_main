@extends('admin.layouts.layout-horizontal')

@section('content')
<div class="main-content" id="ProjectNavigation">
    <div class="page-header">
        <form action="/admin/highwaylist" method="POST" class="form-group">
            @csrf
            <table class="table" id="highwayTable">
                <tr>
                    <td colspan="3">Highway Name</td>
                </tr>
                <tr>
                    <td colspan="3"><input type="text" required id="name" name="highwayname" value="New Project" class="form-control"></td>
                </tr>
                <tr index="1" class="routes">
                    <td>Route Name</td>
                    <td>Chainage/Kilometer From</td>
                    <td>To</td>
                </tr>
                <tr>
                    <td><input required type="text" name="route_1" class="form-control"></td>
                    <td><input required type="text" name="from_1" class="form-control"></td>
                    <td><input required type="text" name="to_1" class="form-control"></td>
                </tr>
            </table>
            <input style="display:none" value="1" name="count" id="highway_count">
            <div class="w-100">
                <div class="float-left">
                    <button type="button" id="addHightWayButton" class="btn btn-info" >Add New</button>
                </div>
                <div class="float-left" style="margin-left: 10px">
                    <input type="submit" class="btn btn-success" value="Save">
                </div>
            </div>
        </form>
        <br>
        <br>
        <br>
        <table class="table" id="highwayTable">
            <tr>
                <td colspan="2">Highway Name</td>
                <td colspan="2">Route Name</td>
                <td colspan="1">From(KM)</td>
                <td colspan="1">To(KM)</td>
            </tr>
            @if ($error != false)
                <script>alert("Already Exist")</script>
            @endif
            @for ($i = 0 ; $i < count($data); $i ++)
                <tr>
                    @if ($i == 0)
                        <td colspan="2">{{$data[$i]->highwayname}}</td>
                    @elseif ($data[$i]->highwayname == $data[$i-1]->highwayname)
                        <td colspan="2"></td>
                    @else  
                        <td colspan="2">{{$data[$i]->highwayname}}</td>
                    @endif
                    <td colspan="2">{{$data[$i]->routename}}</td>
                    <td colspan="1">{{$data[$i]->from}}</td>
                    <td colspan="1">{{$data[$i]->to}}</td>
                </tr>
            @endfor
        </table>
    </div>
</div>
@stop