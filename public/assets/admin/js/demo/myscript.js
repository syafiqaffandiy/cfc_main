$(document).ready(() => {
    $("#addHightWayButton").click(e => {
        index = $(".routes").length + 1;
        var el = "<tr index='" + index + "' class='routes'>";
            el += "<td>Route Name</td>";
            el += "<td>Chainage/Kilometer From</td>";
            el += "<td>To</td>";
            el += "</tr>";
            el += "<tr>";
            el += "<td><input required type='text' name='route_" + index + "' class='form-control'></td>";
            el += "<td><input required type='text' name='from_" + index + "' class='form-control'></td>";
            el += "<td><input required type='text' name='to_" + index + "' class='form-control'></td>";
            el += "</tr>";
        $("#highwayTable").append(el);
        $("#highway_count").val(index);
    });
    initRegisterHighWay = (para, type) => {
        var data = JSON.parse($("#register_variable").text());
        console.log(data)
        if(type == "high"){
            $("#route_register").html("");
            data.forEach(element => {
                if(element.highwayname == para){
                    var el = "<option value='" + element.routename + "'>" + element.routename + "</option>";
                    $("#route_register").append(el);
                }
            });
        } else {
            data.forEach(element => {
                if(element.routename == para){
                    $("#from_register").val(element.from);
                    $("#to_register").val(element.to)
                }
            });
        }
    }
    $("#highway_register").change((e) => {
        console.log(e.target.value);
        initRegisterHighWay(e.target.value, "high");
    });
    $("#route_register").change((e) => {
        console.log(e.target.value);
        initRegisterHighWay(e.target.value, "route");
    });
    $('[data-toggle="tooltip"]').tooltip();
    getScopeData = () => {
        return [
            {
                'scope': 'Scope 1',
                'preplanning': 12,
                'construction': 11,
                'operation_maintenance': 32
            },
            {
                'scope': 'Scope 2',
                'preplanning': 22,
                'construction': 11,
                'operation_maintenance': 44
            },
            {
                'scope': 'Scope 3',
                'preplanning': 11,
                'construction': 66,
                'operation_maintenance': 33
            }
        ]
    }
    AmCharts.makeChart('allRoute_amChart', {
        'type': 'serial',
        'categoryField': 'scope',
        'theme': 'light',
        'rotate': true,
        'startDuration': 1,
        'categoryAxis': {
        'gridPosition': 'start',
        'position': 'left'
        },
        'trendLines': [],
        'graphs': [
        {
            'balloonText': 'Pre-Planning:[[value]]',
            'fillAlphas': 0.8,
            'id': 'AmGraph-1',
            'lineAlpha': 0.2,
            'title': 'PrePlanning',
            'type': 'column',
            'valueField': 'preplanning'
        },
        {
            'balloonText': 'Construction:[[value]]',
            'fillAlphas': 0.8,
            'id': 'AmGraph-2',
            'lineAlpha': 0.2,
            'title': 'Construction',
            'type': 'column',
            'valueField': 'construction'
        },
        {
            'balloonText': 'OperationMaintenance:[[value]]',
            'fillAlphas': 0.8,
            'id': 'AmGraph-3',
            'lineAlpha': 0.2,
            'title': 'Operation',
            'type': 'column',
            'valueField': 'operation_maintenance'
        }
        ],
        'guides': [],
        'valueAxes': [
        {
            'id': 'ValueAxis-1',
            'position': 'top',
            'axisAlpha': 0
        }
        ],
        'allLabels': [],
        'balloon': {},
        'titles': [],
        'dataProvider': getScopeData(),
        'export': {
        'enabled': false
        }
    });
})