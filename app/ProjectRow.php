<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectRow extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'public_id',
        'path',
        'project_id'
    ];

    public function project_row_items() {
        return $this->hasMany('\App\ProjectRowItem');
    }
}
