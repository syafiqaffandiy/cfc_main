<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectRowItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'value',
        'project_row_id'
    ];
}
