<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;


class UserAuditor extends Model
{
    protected $table = "user_auditors";
    public function user_meta()
    {
        return $this->hasOne('App\User','id','user_id');
    }


}
