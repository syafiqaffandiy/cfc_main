<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HighwayList extends Model
{
    public $timestamps = false;

    protected $table = "highwaylist";

    protected $fillable = [
        'highwayname',
        'routename',
        'from',
        'to'
    ];
}