<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmissionFactor extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'category',
        'subcategory',
        'factor',
        'unit',
        'sources',
        'type',
        'project_id'
    ];

    public static $keysToBeFilledByUser = [
        'category',
        'subcategory',
        'factor',
        'unit',
        'sources',
    ];
}
