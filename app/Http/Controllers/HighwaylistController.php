<?php
namespace App\Http\Controllers;

use App\HighwayList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class HighwaylistController extends Controller
{
    public function index()
    {
        // $data = DB::table("highwaylist")->get();

        $data = [
            'data' => $data = DB::table("highwaylist")->get(),
            'error' => false
        ];
        return view('admin.highwaylist.highwaylist', $data);
    }

    public function saveList(Request $request)
    {
        $data = DB::table('highwayList')->get();
        for ($i=0; $i < count($data); $i++) { 
            if ($data[$i]->highwayname == $request->highwayname) {
                return view('admin.highwaylist.highwaylist', array("data" => $data, "error" => true));
            }
        }
        for ($i=1; $i <= $request->count; $i++) { 
            $highway = new HighwayList;
            
            $route = "route_" . $i;
            $from = "from_" . $i;
            $to = "to_" . $i;

            $highway->highwayname = $request->highwayname;
            $highway->routename = $request->$route;
            $highway->from = $request->$from;
            $highway->to = $request->$to;

            $highway->save();
        }
        
        $data = DB::table("highwaylist")->get();
        return redirect('admin/settings/highwaylist')->with('data', $data);
    }
}
