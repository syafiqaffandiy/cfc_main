<?php
namespace App\Http\Controllers;

use App\EmissionFactor;
use App\Evaluator;
use App\Keyword;
use App\KeywordItem;
use App\Project;
use App\ProjectEvaluator;
use App\HighwayList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::all();
        $data = [
            'projects' => $projects
        ];
        return view('admin.project.list', $data);
    }

    public function show($projectId) {
        $project = Project::findOrFail($projectId); /** @var Project $project */
        $data = $this->getProjectForView($project);
        return view('admin.project.get', $data);
    }

    public function store(Request $request) {
        $data = $request->all();
        //echo "<pre>";print_r($data);echo "</pre>"; die;
        if(!empty($data['id'])) {
            $id = $data['id'];
            $project = Project::find($id);
            $assessmentYears = array_merge([$data['assessment_year']], $request->get('assessment_years', []));
            $project->assessment_years = json_encode($assessmentYears);

            $sectionVerification = json_decode($project->section_verification, true);
            $sectionPdf = json_decode($project->section_pdf, true);
            if(empty($sectionVerification)) {
                $sectionVerification = [];
                $sectionPdf = [];
            }
            $projectNavigation = Config::get('project_navigation');
            if(count($sectionVerification) < count($assessmentYears)) {
                // if verifications are less than years, add a new verification section and a new pdf section
                $sectionVerification[] = Project::initSectionVerification([], $projectNavigation);
                $sectionPdf[] = Project::initSectionPdf([], $projectNavigation);
                $project->section_verification = json_encode($sectionVerification);
                $project->section_pdf = json_encode($sectionPdf);
            }

            $project->update($data);
            // now update evaluators
            ProjectEvaluator::where('project_id', '=', $id)->delete();
            if(!empty($data['evaluators'])) {
                foreach ($data['evaluators'] as $evaluator) {
                    ProjectEvaluator::firstOrCreate([
                        'project_id' => $id,
                        'evaluator_id' => $evaluator['id'],
                        'approver' => isset($evaluator['approver']) && $evaluator['approver'] == 'on' ? 1 : 0
                    ]);
                }
            }
        }
        else {
            $data['name'] = 'New Project';
            $data['report_period'] = 1;
            $data['settings'] = '[]';
            // data structures stored 'as-is' because of a matter of time in json format
            $data['construction_header'] = '[]';
            $data['construction_footer'] = '[]';
            $data['construction_work_values'] = '[]';
            $data['assessment_years'] = '[]';
            $data['score_awards_totals'] = "[]";

            $project = Project::create($data);
            $keywords = Keyword::getDataForProjectCreation(true);
            foreach($keywords as $key => $keyword) {
                foreach($keyword as $element) {
                    try {
                        $fillable = [
                            'category' => !empty($element['category']) ? $element['category'] : '',
                            'subcategory' => isset($element['subcategory']) ? $element['subcategory'] : $element['title'],
                            'factor' => isset($element['factor']) ? (float)$element['factor'] : (float)$element['distance'],
                            'unit' => $element['unit'],
                            'sources' => $element['sources'],
                            'type' => Keyword::$emissionFactorKeys[$key],
                            'project_id' => $project->id
                        ];
                        EmissionFactor::create($fillable);
                    }
                    catch(\Exception $e) {
                        error_log("Error creating emission factor for new project: ".$e->getMessage());
                    }
                }
            }
        }
        $data = $this->getProjectForView($project);
        return redirect('admin/projects/'.$project->id)->with('data', $data);
    }

    public function update(Request $request) {
        $requestData = $request->all();
        $postData = $requestData['data'];
        Project::storeProjectStructure($postData);
        return \Illuminate\Support\Facades\Response::json(['status' => 'OK'], 200);
    }

    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();
        flash('Project Deleted')->success();

        return redirect()->back();
    }

    public function deleteAssessmentYear(Request $request) {
        $yearToDelete = $request->get('delete');
        $project = Project::find($request->get('project_id'));
        if(empty($yearToDelete)) {
            throw new Exception();
        }
        $assessmentYears = json_decode($project->assessment_years);
        $settings = json_decode($project->settings, true);
        $sectionVerification = json_decode($project->section_verification, true);
        $sectionPdf = json_decode($project->section_pdf, true);
        $scoreAwardsTotals = json_decode($project->score_awards_totals, true);
        if($yearToDelete == $assessmentYears[0]) {
            throw new Exception("Unable to delete first assessment year");
        }
        foreach($assessmentYears as $key => $value) {
            if($key == $yearToDelete) {
                unset($assessmentYears[$key]);
                unset($settings[$key]);
                unset($sectionVerification[$key]);
                unset($sectionPdf[$key]);
                foreach($scoreAwardsTotals as $scoreKey => $scoreValue) {
                    if($scoreValue['year'] == $value) {
                        unset($scoreAwardsTotals[$scoreKey]);
                        break 2;
                    }
                }
            }
        }
        $project->assessment_years = json_encode($assessmentYears);
        $project->settings = json_encode($settings);
        $project->section_verification = json_encode($sectionVerification);
        $project->section_pdf = json_encode($sectionPdf);
        $project->score_awards_totals = json_encode($scoreAwardsTotals);
        $project->save();

        return redirect('admin/projects/'.$project->id);
    }

    /**
     * @param Project $project
     * @return array
     */
    private function getProjectForView($project)
    {
        $evaluators = Evaluator::select(['id', 'name as title', 'id as option'])->get(['id', 'title', 'option'])->getDictionary();
        $data = Keyword::getGlobalData();
        $data['evaluators'] = $evaluators;
        $project->structure = json_encode($project->getProjectSettingsStructure());
        $projectNavigation = Config::get('project_navigation');
        if(!isset($project->section_verification)) {
            $project->section_verification = json_encode(Project::initSectionVerification([], $projectNavigation));
        }
        if(empty($project->section_pdf) || empty(json_decode($project->section_pdf))) {
            $project->section_pdf = json_encode(Project::initSectionPdf([], $projectNavigation));
        }
        if(empty($project->assessment_years)) {
            $project->assessment_years = [$project->assessment_year];
        }
        else {
            $project->assessment_years = json_decode($project->assessment_years);
        }
        if(empty($project->score_awards_totals)) {
            $project->score_awards_totals = "[]";
        }
        $highways = DB::table("highwaylist")->get();
        $data = [
            'sheet_structure' => Config::get('project_navigation'),
            'global_data' => $data,
            'project' => $project,
            'highwaylist' => $highways
        ];
        return $data;
    }
}
