<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;


class UserConcessionaire extends Model
{
    protected $table = "user_concessionaire";
    public function user_meta()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function person_charge()
    {
        return $this->hasOne('App\User','id','person_in_charge');
    }
}
