<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeywordItem extends Model
{
    public $timestamps = false;

    public $fillable = [
        'keyword_id',
        'name',
        'value'
    ];
}
