<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluator extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];
}
