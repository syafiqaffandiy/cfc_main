<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectEvaluator extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'project_id',
        'evaluator_id',
        'approver'
    ];

    public function evaluator() {
        return $this->belongsTo('\App\Evaluator', 'evaluator_id');
    }
}
